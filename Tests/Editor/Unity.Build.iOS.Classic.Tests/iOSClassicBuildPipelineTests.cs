using NUnit.Framework;
using Unity.Build.Classic.Private;

namespace Unity.Build.iOS.Classic.Tests
{
    [TestFixture]
    class iOSClassicBuildPipelineTests
    {
        [Test]
        public void BuildPipelineSelectorTests()
        {
            var selector = new BuildPipelineSelector();
            Assert.That(selector.SelectFor(Platform.iOS).GetType(), Is.EqualTo(typeof(MissingClassicNonIncrementalPipeline)));
        }
    }
}
