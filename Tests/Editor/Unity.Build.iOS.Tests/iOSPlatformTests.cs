using NUnit.Framework;
using Unity.Build.Editor;
using Unity.Serialization.Json;

namespace Unity.Build.iOS.Tests
{
    class iOSPlatformTests
    {
        [Test]
        public void iOSPlatform_Equals()
        {
            var platform = new iOSPlatform();
            Assert.That(platform, Is.EqualTo(Platform.iOS));
        }

        [Test]
        public void iOSPlatform_GetIcon()
        {
            Assert.That(Platform.iOS.GetIcon(), Is.Not.Null);
        }

        [Test]
        public void iOSPlatform_Serialization()
        {
            var serialized = JsonSerialization.ToJson(Platform.iOS);
            var deserialized = JsonSerialization.FromJson<Platform>(serialized);
            Assert.That(deserialized, Is.EqualTo(Platform.iOS));
        }
    }
}
