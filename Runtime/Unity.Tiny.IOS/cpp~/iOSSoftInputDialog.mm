#include <Unity/Runtime.h>

#include <vector>
#include <mutex>
#include <IOSWrapper.h>

static std::vector<char16_t> inputString;
static bool inputStringUpdated;
static int inputStart;
static int inputLength;
static bool inputActive;
static bool inputCanceled;
static int keyboardX, keyboardY, keyboardWidth, keyboardHeight;
static std::mutex softInputLock;

void convert_to_window(int *x, int *y);

extern "C" void UnityKeyboard_Create(unsigned keyboardType, int autocorrection, int multiline, int secure,
                                     const unichar* text, int textLength, const unichar* placeholder, int placeholderLength, int characterLimit);
extern "C" void UnityKeyboard_Show();
extern "C" void UnityKeyboard_Hide();
extern "C" void UnityKeyboard_SetText(const unichar* text, int length);
extern "C" NSString* UnityKeyboard_GetText();
extern "C" int UnityKeyboard_IsActive();
extern "C" int UnityKeyboard_Status();
extern "C" void UnityKeyboard_SetInputHidden(int hidden);
extern "C" int UnityKeyboard_IsInputHidden();
extern "C" void UnityKeyboard_GetRect(int* x, int* y, int* w, int* h);
extern "C" void UnityKeyboard_SetCharacterLimit(unsigned characterLimit);
extern "C" int UnityKeyboard_CanGetSelection();
extern "C" void UnityKeyboard_GetSelection(int* location, int* length);
extern "C" int UnityKeyboard_CanSetSelection();
extern "C" void UnityKeyboard_SetSelection(int location, int length);


DOTS_EXPORT(void)
show_soft_input_ios(char16_t* initialText, int initialTextLength, int type,
                        bool correction, bool multiline, bool secure,
                        char16_t* placeholder, int placeholderLength, int characterLimit, bool isInputFieldHidden,
                        int selectionStart, int selectionLength)
{
    {
        std::lock_guard<std::mutex> lock(softInputLock);
        inputString.resize(initialTextLength);
        memcpy(inputString.data(), initialText, initialTextLength * sizeof(char16_t));
        inputStart = selectionStart;
        inputLength = selectionLength;
        inputActive = true;
        inputCanceled = false;
        inputStringUpdated = false;
    }
    UnityKeyboard_Create(type, correction, multiline, secure, (const unichar*)initialText, initialTextLength, (const unichar*)placeholder, placeholderLength, characterLimit);
    UnityKeyboard_Show();
    UnityKeyboard_SetInputHidden(isInputFieldHidden);
    if (UnityKeyboard_CanSetSelection())
    {
        UnityKeyboard_SetSelection(inputStart, inputLength);
    }
}

DOTS_EXPORT(void)
hide_soft_input_ios()
{
    UnityKeyboard_Hide();
}

DOTS_EXPORT(void)
set_soft_input_string_ios(char16_t* text, int length)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    UnityKeyboard_SetText((const unichar*)text, length);
    inputString.resize(length);
    memcpy(inputString.data(), text, length * sizeof(char16_t));
    inputStringUpdated = false;
}

DOTS_EXPORT(const char16_t*)
get_soft_input_string_ios(int *len, bool *updated)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    if (len == NULL || updated == NULL)
        return NULL;
    *len = (int)inputString.size();
    *updated = inputStringUpdated;
    inputStringUpdated = false;
    return inputString.data();
}

DOTS_EXPORT(void)
set_character_limit_ios(int limit)
{
    UnityKeyboard_SetCharacterLimit((unsigned)limit);
}

DOTS_EXPORT(void)
set_input_selection_ios(int start, int length)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    if (inputStart == start && inputLength == length)
        return;
    if (UnityKeyboard_CanSetSelection())
    {
        UnityKeyboard_SetSelection(start, length);
    }
    inputStart = start;
    inputLength = length;
}

DOTS_EXPORT(bool)
get_input_selection_ios(int *start, int *len)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    if (start == NULL || len == NULL)
        return false;
    if (!UnityKeyboard_CanGetSelection())
    {
        *start = inputStart;
        *len = inputLength;
        return false;
    }
    UnityKeyboard_GetSelection(start, len);
    if (*start == inputStart && *len == inputLength)
        return false;
    inputStart = *start;
    inputLength = *len;
    return true;
}

DOTS_EXPORT(void)
set_hide_input_field_ios(bool hidden)
{
    UnityKeyboard_SetInputHidden(hidden);
}

DOTS_EXPORT(bool)
get_input_area_ios(bool *visible, int *x, int *y, int *width, int *height)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    if (visible == NULL || x == NULL || y == NULL || width == NULL || height == NULL)
        return false;
    *visible = inputActive;
    UnityKeyboard_GetRect(x, y, width, height);
    int left = *x;
    int right = *x + *width;
    int top = *y;
    int bottom = *y + *height - 1;
    convert_to_window(&left, &bottom);
    convert_to_window(&right, &top);
    *x = left;
    *y = bottom;
    *width = right - left;
    *height = top - bottom;
    if (*x == keyboardX && *y == keyboardY && *width == keyboardWidth && *height == keyboardHeight)
        return false;
    keyboardX = *x;
    keyboardY = *y;
    keyboardWidth = *width;
    keyboardHeight = *height;
    return true;
}

DOTS_EXPORT(void)
get_soft_input_state_ios(bool *active, bool *canceled)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    if (active == NULL || canceled == NULL)
        return;
    *active = inputActive;
    *canceled = inputCanceled;
}

DOTS_EXPORT(void)
UnityKeyboard_TextChanged(NSString* text)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    int length = [text length];
    inputString.resize(length);
    [text getCharacters: (unichar*)inputString.data() range: NSMakeRange(0, length)];
    inputStringUpdated = true;
}

DOTS_EXPORT(void)
UnityKeyboard_LayoutChanged(NSString* layout)
{
    // TODO check, implement if required
}

DOTS_EXPORT(void)
UnityKeyboard_StatusChanged(int status)
{
    std::lock_guard<std::mutex> lock(softInputLock);
    inputActive = (status == 0); // "Visible"
    inputCanceled = (status > 1); // not "Done";
}
