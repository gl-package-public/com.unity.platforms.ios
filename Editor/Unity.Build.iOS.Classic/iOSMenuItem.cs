﻿using Unity.Build.Classic;
using Unity.Build.Editor;
using UnityEditor;

namespace Unity.Build.iOS.Classic
{
    static class iOSMenuItem
    {
        [MenuItem(BuildConfigurationMenuItem.k_MenuPathName + KnownPlatforms.iOS.DisplayName + ClassicBuildConfigurationMenuItem.k_ItemNameSuffix)]
        static void CreateClassicBuildConfigurationAsset()
        {
            ClassicBuildConfigurationMenuItem.CreateAssetInActiveDirectory(Platform.iOS);
        }
    }
}
